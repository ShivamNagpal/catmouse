import java.util.Random;

public class Main {
    final static int UP = 1, DOWN = 2, LEFT = 3, RIGHT = 4;
    static char[][] maze;
    static int mazeX;
    static int mazeY;
    static Dim food;
    static Dim cat;
    static Dim mouse;
    static boolean lost = false;
    static int catNotAllowed;
    static int mouseNotAllowed;
    private static Random random;
    private static boolean won;


    public static void main(String[] args) {
        random = new Random();
        initMaze();
        initFood();
        initCat();
        initMouse();
        show_maze();
        run();
    }

    private static void initFood() {
        int x = random.nextInt(mazeX);
        int y = random.nextInt(mazeY);
        while (!isBlank(x, y)) {
            x = random.nextInt(mazeX);
            y = random.nextInt(mazeY);
        }
        food = new Dim(x, y);
    }


    private static void initCat() {
        int x = random.nextInt(mazeX);
        int y = random.nextInt(mazeY);
        while (!isBlank(x, y)) {
            x = random.nextInt(mazeX);
            y = random.nextInt(mazeY);
        }
        cat = new Dim(x, y);
    }

    private static void initMouse() {
        int x = random.nextInt(mazeX);
        int y = random.nextInt(mazeY);
        while (!isBlank(x, y)) {
            x = random.nextInt(mazeX);
            y = random.nextInt(mazeY);
        }
        mouse = new Dim(x, y);
    }

    private static boolean isBlank(int x, int y) {
        return maze[x][y] == '.';
    }

    private static boolean isBlank(int x, int y, int dir) {
        switch (dir) {
            case UP:
                return maze[x - 1][y] == '.';
            case DOWN:
                return maze[x + 1][y] == '.';
            case LEFT:
                return maze[x][y - 1] == '.';
            case RIGHT:
                return maze[x][y + 1] == '.';
        }
        return false;
    }

    static void show_maze() {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                if (i == food.x && j == food.y) {
                    System.out.print("F ");
                } else if (i == cat.x && j == cat.y) {
                    System.out.print("C ");
                } else if (i == mouse.x && j == mouse.y) {
                    System.out.print("M ");
                } else {
                    System.out.print(maze[i][j] + " ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    static void initMaze() {
        maze = new char[][]{
                {'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b' },
                {'b', '.', '.', '.', '.', '.', '.', '.', 'b', 'b', '.', '.', '.', 'b' },
                {'b', '.', 'b', 'b', '.', '.', '.', '.', '.', 'b', '.', 'b', '.', 'b' },
                {'b', '.', 'b', '.', '.', 'b', 'b', '.', '.', '.', '.', 'b', '.', 'b' },
                {'b', '.', 'b', '.', '.', 'b', 'b', '.', '.', '.', '.', 'b', '.', 'b' },
                {'b', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', 'b' },
                {'b', '.', '.', '.', '.', '.', '.', '.', 'b', '.', '.', '.', '.', 'b' },
                {'b', '.', 'b', 'b', 'b', '.', '.', 'b', 'b', 'b', '.', '.', '.', 'b' },
                {'b', '.', '.', 'b', 'b', '.', '.', '.', 'b', '.', '.', 'b', '.', 'b' },
                {'b', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', 'b' },
                {'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b' }
        };
        mazeX = maze.length;
        mazeY = maze[0].length;
    }

    static void run() {
        while (true) {
            catMoveNext();
            if (lost) {
                System.out.println("Lost: Cat ate Mouse");
                break;
            }
            mouseMoveNext();
            if(won) {
                System.out.println("Won: Mouse ate Food");
                break;
            }
            show_maze();
        }
    }

    static void mouseMoveNext() {
        int min = 1000;
        int dir = 0;
        if (isBlank(mouse.x - 1, mouse.y)) {
            int moves = MinMoves.minimumMoves(mouse.x - 1, mouse.y, food.x, food.y);
            if (moves < min) {
                dir = UP;
                min = moves;
            }
        }
        if (isBlank(mouse.x + 1, mouse.y)) {
            int moves = MinMoves.minimumMoves(mouse.x + 1, mouse.y, food.x, food.y);
            if (moves < min) {
                dir = DOWN;
                min = moves;
            }
        }
        if (isBlank(mouse.x, mouse.y - 1)) {
            int moves = MinMoves.minimumMoves(mouse.x, mouse.y - 1, food.x, food.y);
            if (moves < min) {
                dir = LEFT;
                min = moves;
            }
        }
        if (isBlank(mouse.x, mouse.y + 1)) {
            int moves = MinMoves.minimumMoves(mouse.x, mouse.y, food.x, food.y);
            if (moves < min) {
                dir = RIGHT;
                min = moves;
            }
        }

        while (dir == mouseNotAllowed) {
            int x = 1 + random.nextInt(4);
            if (isBlank(mouse.x, mouse.y, x)) {
                dir = x;
            }
        }

        switch (dir) {
            case UP:
                mouse.x -= 1;
                mouseNotAllowed = DOWN;
                break;
            case DOWN:
                mouse.x += 1;
                mouseNotAllowed = UP;
                break;
            case LEFT:
                mouse.y -= 1;
                mouseNotAllowed = RIGHT;
                break;
            case RIGHT:
                mouse.y += 1;
                mouseNotAllowed = LEFT;
                break;
        }


        if (mouse.equals(food)) {
            won = true;
        }
    }


    static void catMoveNext() {
        int min = 1000;
        int dir = 0;
        if (isBlank(cat.x - 1, cat.y)) {
            int moves = MinMoves.minimumMoves(cat.x - 1, cat.y, mouse.x, mouse.y);
            if (moves < min) {
                dir = UP;
                min = moves;
            }
        }
        if (isBlank(cat.x + 1, cat.y)) {
            int moves = MinMoves.minimumMoves(cat.x + 1, cat.y, mouse.x, mouse.y);
            if (moves < min) {
                dir = DOWN;
                min = moves;
            }
        }
        if (isBlank(cat.x, cat.y - 1)) {
            int moves = MinMoves.minimumMoves(cat.x, cat.y - 1, mouse.x, mouse.y);
            if (moves < min) {
                dir = LEFT;
                min = moves;
            }
        }
        if (isBlank(cat.x, cat.y + 1)) {
            int moves = MinMoves.minimumMoves(cat.x, cat.y, mouse.x, mouse.y);
            if (moves < min) {
                dir = RIGHT;
                min = moves;
            }
        }

        while (dir == catNotAllowed) {
            int x = 1 + random.nextInt(4);
            if (isBlank(cat.x, cat.y, x)) {
                dir = x;
            }
        }

        switch (dir) {
            case UP:
                cat.x -= 1;
                catNotAllowed = DOWN;
                break;
            case DOWN:
                cat.x += 1;
                catNotAllowed = UP;
                break;
            case LEFT:
                cat.y -= 1;
                catNotAllowed = RIGHT;
                break;
            case RIGHT:
                cat.y += 1;
                catNotAllowed = LEFT;
                break;
        }


        if (cat.equals(mouse)) {
            lost = true;
        }
    }
}
