import java.util.LinkedList;
import java.util.Queue;

public class MinMoves {

    static int[][] game;
    static boolean[][] visited;
    static Queue<Dim> queue = new LinkedList<>();


    static int minimumMoves(int startX, int startY, int goalX, int goalY) {
        game = new int[Main.maze.length][Main.maze[0].length];
        visited = new boolean[Main.maze.length][Main.maze[0].length];

        for(int i = 0; i < Main.maze.length; i++) {
            for(int j=0; j < Main.maze[0].length; j++) {
                if(Main.maze[i][j] == '.') {
                    game[i][j] = 100;
                } else {
                    game[i][j] = -1;
                }
            }
        }

        queue.add(new Dim(startX, startY));
        game[startX][startY] = 0;

        while(!queue.isEmpty()) {
            Dim p = queue.remove();
            int x = p.x, y = p.y;
            if(!visited[x][y]) {
                visited[x][y] = true;
                moveGenerator(x, y);
            }
        }
        return game[goalX][goalY];
    }

    static void moveGenerator(int x, int y) {
        for(int i=x; i<game.length && game[i][y] != -1; i++) {
            queue.add(new Dim(i, y));
            updateValue(i, y, game[x][y]);
        }

        for(int i = x; i >= 0 && game[i][y] != -1; i--) {
            queue.add(new Dim(i, y));
            updateValue(i, y, game[x][y]);
        }

        for(int j = y; j < game.length && game[x][j] != -1; j++) {
            queue.add(new Dim(x, j));
            updateValue(x, j, game[x][y]);
        }

        for(int j = y; j >= 0 && game[x][j] != -1; j--) {
            queue.add(new Dim(x, j));
            updateValue(x, j, game[x][y]);
        }
    }

    static void updateValue(int x, int y, int value) {
        if(game[x][y] > value + 1) {
            game[x][y] = value + 1;
        }

    }
}
