public class Dim {
    int x;
    int y;

    public Dim(int x, int y) {
        this.x = x;
        this.y = y;
    }

    boolean equals(Dim dim) {
        return (this.x == dim.x && this.y == dim.y);
    }
}
